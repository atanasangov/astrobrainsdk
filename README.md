### How do I use it ###
```
$brain = new AstroBrainSDK($clientId, $apiKey);
$brain->setDateOfBirth("08.05.1989");
$brain->setTimeOfBirth("11:27:19");
$brain->setPlaceOfBirth("Buenos Aires, Argentina");
$birthChartUrl = json_decode($brain->send());
var_dump($birthChartUrl->url);
```

### How to use the transits SDK
```
$brain = new AstroBrainSDK($clientId, $apiKey);
$brain->setDateOfBirth("08.05.1989");
$brain->setTimeOfBirth("11:27:19");
$brain->setPlaceOfBirth("Buenos Aires, Argentina");
$brain->setTransitDateOfBirth("18.07.2020");
$brain->setTransitTimeOfBirth("13:02:22");
$brain->setTransitPlaceOfBirth("Sofia, Bulgaria");
$birthChartUrl = json_decode($brain->send());
var_dump($birthChartUrl->url);
```
