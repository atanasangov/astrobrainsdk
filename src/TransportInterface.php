<?php
/**
 * @author Atanas Angov <atanas@angov.org>
 * Date: 18.04.20 г.
 * Time: 11:16 ч.
 */

namespace AstroBrainSDK;


interface TransportInterface
{
    /**
     * Sets the data on the transport layer
     *
     * @param array $data
     */
    public function setData(array $data): void;

    /**
     * Sends the request and returns the image URL
     *
     * @return string
     */
    public function send(): string;
}