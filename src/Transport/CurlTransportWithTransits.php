<?php
/**
 * @author Atanas Angov <atanas@angov.org>
 * Date: 18.07.20 г.
 * Time: 12:39 ч.
 */

namespace AstroBrainSDK\Transport;


use AstroBrainSDK\TransportInterface;

class CurlTransportWithTransits implements TransportInterface
{
    /** @var array */
    private $data;

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function send(): string
    {
        $ch = curl_init();
        $url = $this->data['url'];
        $url .= curl_escape($ch, 'dob=' . $this->data['dob']);
        $url .= curl_escape($ch, '&tob=' . $this->data['tob']);
        $url .= curl_escape($ch, '&pob=' . $this->data['pob']);
        $url .= curl_escape($ch, '&transitDob=' . $this->data['transitDateOfBirth']);
        $url .= curl_escape($ch, '&transitTob=' . $this->data['transitTimeOfBirth']);
        $url .= curl_escape($ch, '&transitPob=' . $this->data['transitPlaceOfBirth']);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERPWD, $this->data['clientId'] . ":" . $this->data['apiKey']);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if(curl_errno($ch)){
            var_dump("Error: " . curl_error($ch));
        }
        curl_close($ch);

        return $response;
    }
}