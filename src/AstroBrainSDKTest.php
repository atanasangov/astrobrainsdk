<?php
/**
 * @author Atanas Angov <atanas@angov.org>
 * Date: 18.04.20 г.
 * Time: 10:41 ч.
 */

namespace AstroBrainSDK;


use AstroBrainSDK\Exceptions\InvalidDataException;

class AstroBrainSDKTest extends AstroBrainBaseTest
{
    /** @var TransportInterface */
    private $transportMock;
    /** @var string */
    private $clientId;
    /** @var string */
    private $apiKey;
    /** @var AstroBrainSDK */
    private $sdk;

    protected function setUp(): void
    {
        parent::setUp();
        $this->clientId = 'clientId' . md5(time());
        $this->apiKey = 'apiKey' . md5(time());
        $this->transportMock = $this->getMockBuilder(TransportInterface::class)->disableOriginalConstructor()->getMock();
        $this->sdk = new AstroBrainSDK($this->clientId, $this->apiKey, $this->transportMock);
    }

    public function testInitializesWithClientIdAndApiKey()
    {
        [$dob, $tob, $placeOfBirth] = $this->setData();

        $this->assertSame($this->clientId, $this->sdk->getClientId());
        $this->assertSame($this->apiKey, $this->sdk->getApiKey());
        $this->assertSame($dob, $this->sdk->getDateOfBirth());
        $this->assertSame($tob, $this->sdk->getTimeOfBirth());
        $this->assertSame($placeOfBirth, $this->sdk->getPlaceOfBirth());
    }

    public function testSendWithEmptyDataThrowsException()
    {
        $this->expectException(InvalidDataException::class);
        $this->sdk->send();
    }

    public function testSend()
    {
        [$dob, $tob, $placeOfBirth] = $this->setData();
        $imgUrl = 'http://alabala.protokalaa.net/imagehash.png';
        $params = [
            'url' => 'http://astrobrain.angov.org/api/v1/',
            'clientId' => $this->clientId,
            'apiKey' => $this->apiKey,
            'dob' => $dob,
            'tob' => $tob,
            'pob' => $placeOfBirth,
        ];

        $this->transportMock->expects($this->once())->method('setData')->with($params);
        $this->transportMock->expects($this->once())->method('send')->willReturn($imgUrl);

        echo $this->sdk->send();
    }

    /**
     * @return array|string[]
     */
    private function setData(): array
    {
        $dob = '13.05.1982';
        $this->sdk->setDateOfBirth($dob);
        $tob = '13:22:22';
        $this->sdk->setTimeOfBirth($tob);
        $placeOfBirth = 'Haskovo, Bulgaria';
        $this->sdk->setPlaceOfBirth($placeOfBirth);
        return array($dob, $tob, $placeOfBirth);
    }
}