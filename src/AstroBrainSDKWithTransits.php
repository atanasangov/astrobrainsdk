<?php
/**
 * @author Atanas Angov <atanas@angov.org>
 * Date: 18.07.20 г.
 * Time: 12:26 ч.
 */

namespace AstroBrainSDK;


use AstroBrainSDK\Exceptions\InvalidDataException;
use AstroBrainSDK\Transport\CurlTransportWithTransits;

class AstroBrainSDKWithTransits extends AstroBrainSDK
{
    protected $transitDateOfBirth;
    protected $transitTimeOfBirth;
    protected $transitPlaceOfBirth;

    public function setTransitDateOfBirth(string $transitDateOfBirth): void
    {
        $this->transitDateOfBirth = $transitDateOfBirth;
    }

    public function setTransitTimeOfBirth(string $transitTimeOfBirth): void
    {
        $this->transitTimeOfBirth = $transitTimeOfBirth;
    }

    public function setTransitPlaceOfBirth(string $transitPlaceOfBirth): void
    {
        $this->transitPlaceOfBirth = $transitPlaceOfBirth;
    }

    /**
     * @return array
     * @throws InvalidDataException
     */
    protected function prepareData(): array
    {
        $params = parent::prepareData();

        $params['transitDateOfBirth'] = $this->transitDateOfBirth;
        $params['transitTimeOfBirth'] = $this->transitTimeOfBirth;
        $params['transitPlaceOfBirth'] = $this->transitPlaceOfBirth;

        return $params;
    }

    protected function validData()
    {
        return !empty($this->transitDateOfBirth) && !empty($this->transitPlaceOfBirth) && !empty($this->transitTimeOfBirth);
    }

    /**
     * @param TransportInterface|null $transport
     */
    protected function setTransport(?TransportInterface $transport): void
    {
        $this->transport = $transport ?? new CurlTransportWithTransits();
    }
}