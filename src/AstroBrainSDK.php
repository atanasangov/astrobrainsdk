<?php
/**
 * @author Atanas Angov <atanas@angov.org>
 * Date: 18.04.20 г.
 * Time: 10:43 ч.
 */

namespace AstroBrainSDK;


use AstroBrainSDK\Exceptions\InvalidDataException;
use AstroBrainSDK\Transport\CurlTransport;

class AstroBrainSDK
{
    /** @var string */
    protected $url = 'http://astrobrain.angov.org/api/v1/';
    /** @var TransportInterface */
    protected $transport;
    /** @var string */
    protected $pob;
    /** @var string */
    protected $tob;
    /** @var string */
    protected $dob;
    /** @var string */
    protected $apiKey;
    /** @var string */
    protected $clientId;

    /**
     * AstroBrainSDK constructor.
     *
     * @param string             $clientId
     * @param string             $apiKey
     * @param TransportInterface $transport
     */
    public function __construct(string $clientId, string $apiKey, TransportInterface $transport = null)
    {
        $this->clientId = $clientId;
        $this->apiKey = $apiKey;
        $this->setTransport($transport);
    }

    /**
     * @return mixed
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    public function setDateOfBirth(string $dob): void
    {
        $this->dob = $dob;
    }

    public function getDateOfBirth(): string
    {
        return $this->dob;
    }

    public function setTimeOfBirth(string $tob): void
    {
        $this->tob = $tob;
    }

    /**
     * @return string
     */
    public function getTimeOfBirth(): string
    {
        return $this->tob;
    }

    public function setPlaceOfBirth(string $placeOfBirth): void
    {
        $this->pob = $placeOfBirth;
    }

    /**
     * @return string
     */
    public function getPlaceOfBirth(): string
    {
        return $this->pob;
    }

    public function send()
    {
        $params = $this->prepareData();
        $this->transport->setData($params);
        return $this->transport->send();
    }

    protected function validData()
    {
        return !empty($this->tob) && !empty($this->pob) && !empty($this->dob);
    }

    /**
     * @return array
     * @throws InvalidDataException
     */
    protected function prepareData(): array
    {
        if (!$this->validData()) {
            throw new InvalidDataException();
        }
        return [
            'url' => $this->url,
            'clientId' => $this->clientId,
            'apiKey' => $this->apiKey,
            'dob' => $this->dob,
            'tob' => $this->tob,
            'pob' => $this->pob,
        ];
    }

    /**
     * @param TransportInterface|null $transport
     */
    protected function setTransport(?TransportInterface $transport): void
    {
        $this->transport = $transport ?? new CurlTransport();
    }
}